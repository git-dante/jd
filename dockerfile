FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build
WORKDIR /app

# copy csproj and restore as distinct layers
COPY app/*.sln .
COPY app/. ./
RUN dotnet restore
WORKDIR /app/JoyDesk
RUN dotnet add package Microsoft.Extensions.Hosting.Abstractions --version 2.2.0
WORKDIR /app
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/core/aspnet:2.2 AS runtime
ARG PKG_VER
ENV JD_VERSION $PKG_VER
WORKDIR /app
COPY --from=build /app/JoyDesk/out ./
ENTRYPOINT ["dotnet", "JoyDesk.dll"]
